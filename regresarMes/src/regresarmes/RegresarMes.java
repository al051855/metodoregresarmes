/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regresarmes;

import java.util.Scanner;

/**
 *
 * @author juanm
 */
public class RegresarMes {

    static Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //
        int mes;
        int resp;
        boolean fullName = false;

        //descomentar esto
        /*
        for (int b = 0; b < 2; b++) {
            for (int m = 0; m < 14; m++) {
         */
        
        System.out.println("=====================================");
        System.out.println("Ingresar el numero del mes, 1-12");

        mes = sc.nextInt();  //comentar esta
        /*System.out.println(m);  //descometar estas 2
                mes = m;*/

        System.out.println("Ingresar si debe ser nombre completo o abreviado, 0-1");

        resp = sc.nextInt();  //comentar
        /*System.out.println(b);  // descomentar estas otras 2
                resp = b;*/

        switch (resp) {

            case 0:
                fullName = false;
                break;
            case 1:
                fullName = true;
                break;
            default:
                System.out.println("Opcion de nombre no existe");
        }

        System.out.println(getMes(mes, fullName));
        /*try {
                    System.out.println("fullName vale " + fullName);
                } catch (RuntimeException x) {
                    System.out.println(mes + ": " + x);
                    continue;
                }*/
        
        //y estos 2 }
        //}
        //}

        System.out.println("Juan Carlos Pinzon Medina - ISC - 2A - 51855");
    }

    private static String getMes(int m, boolean fN) {
        String mes;
        switch (m) {
            case 1:
                if (fN) {
                    mes = "Enero";
                } else {
                    mes = "Ene";
                }
                break;
            case 2:
                if (fN) {
                    mes = "Febrero";
                } else {
                    mes = "Feb";
                }
                break;
            case 3:
                if (fN) {
                    mes = "Marzo";
                } else {
                    mes = "Mar";
                }
                break;
            case 4:
                if (fN) {
                    mes = "Abril";
                } else {
                    mes = "Abr";
                }
                break;
            case 5:
                if (fN) {
                    mes = "Mayo";
                } else {
                    mes = "May";
                }
                break;
            case 6:
                if (fN) {
                    mes = "Junio";
                } else {
                    mes = "Jun";
                }
                break;
            case 7:
                if (fN) {
                    mes = "Julio";
                } else {
                    mes = "Jul";
                }
                break;
            case 8:
                if (fN) {
                    mes = "Agosto";
                } else {
                    mes = "Ago";
                }
                break;
            case 9:
                if (fN) {
                    mes = "Septiembre";
                } else {
                    mes = "Sep";
                }
                break;
            case 10:
                if (fN) {
                    mes = "Octubre";
                } else {
                    mes = "Oct";
                }
                break;
            case 11:
                if (fN) {
                    mes = "Noviembre";
                } else {
                    mes = "Nov";
                }
                break;
            case 12:
                if (fN) {
                    mes = "Diciembre";
                } else {
                    mes = "Dic";
                }
                break;

            default:
                mes = "";
                System.out.println("Mes ingresado no existe");

        }

        return mes;
    }

}
